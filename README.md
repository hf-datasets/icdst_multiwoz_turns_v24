---
dataset_info:
  features:
  - name: dialogue_id
    dtype: string
  - name: turn_id
    dtype: int8
  - name: domains
    sequence: string
  - name: user_utterances
    sequence: string
  - name: system_utterances
    sequence: string
  - name: slot_values
    struct:
    - name: hotel
      struct:
      - name: price range
        dtype: string
      - name: type
        dtype: string
      - name: parking
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book stay
        dtype: string
      - name: stars
        dtype: string
      - name: internet
        dtype: string
      - name: name
        dtype: string
      - name: area
        dtype: string
    - name: train
      struct:
      - name: arrive by
        dtype: string
      - name: departure
        dtype: string
      - name: day
        dtype: string
      - name: book people
        dtype: string
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
    - name: attraction
      struct:
      - name: area
        dtype: string
      - name: name
        dtype: string
      - name: type
        dtype: string
    - name: restaurant
      struct:
      - name: price range
        dtype: string
      - name: area
        dtype: string
      - name: food
        dtype: string
      - name: name
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book time
        dtype: string
    - name: taxi
      struct:
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
      - name: departure
        dtype: string
      - name: arrive by
        dtype: string
  - name: turn_slot_values
    struct:
    - name: hotel
      struct:
      - name: price range
        dtype: string
      - name: type
        dtype: string
      - name: parking
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book stay
        dtype: string
      - name: stars
        dtype: string
      - name: internet
        dtype: string
      - name: name
        dtype: string
      - name: area
        dtype: string
    - name: train
      struct:
      - name: arrive by
        dtype: string
      - name: departure
        dtype: string
      - name: day
        dtype: string
      - name: book people
        dtype: string
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
    - name: attraction
      struct:
      - name: area
        dtype: string
      - name: name
        dtype: string
      - name: type
        dtype: string
    - name: restaurant
      struct:
      - name: price range
        dtype: string
      - name: area
        dtype: string
      - name: food
        dtype: string
      - name: name
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book time
        dtype: string
    - name: taxi
      struct:
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
      - name: departure
        dtype: string
      - name: arrive by
        dtype: string
  - name: last_slot_values
    struct:
    - name: hotel
      struct:
      - name: price range
        dtype: string
      - name: type
        dtype: string
      - name: parking
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book stay
        dtype: string
      - name: stars
        dtype: string
      - name: internet
        dtype: string
      - name: name
        dtype: string
      - name: area
        dtype: string
    - name: train
      struct:
      - name: arrive by
        dtype: string
      - name: departure
        dtype: string
      - name: day
        dtype: string
      - name: book people
        dtype: string
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
    - name: attraction
      struct:
      - name: area
        dtype: string
      - name: name
        dtype: string
      - name: type
        dtype: string
    - name: restaurant
      struct:
      - name: price range
        dtype: string
      - name: area
        dtype: string
      - name: food
        dtype: string
      - name: name
        dtype: string
      - name: book day
        dtype: string
      - name: book people
        dtype: string
      - name: book time
        dtype: string
    - name: taxi
      struct:
      - name: leave at
        dtype: string
      - name: destination
        dtype: string
      - name: departure
        dtype: string
      - name: arrive by
        dtype: string
  - name: system_response_acts
    sequence: string
  - name: system_response
    dtype: string
  splits:
  - name: train
    num_bytes: 78112115
    num_examples: 54971
  - name: validation
    num_bytes: 10725891
    num_examples: 7374
  - name: test
    num_bytes: 10734111
    num_examples: 7368
  - name: valid_20p_ablation
    num_bytes: 2104741.561838893
    num_examples: 1447
  - name: valid_10p
    num_bytes: 1063279.9458909682
    num_examples: 731
  - name: valid_50p
    num_bytes: 5378945.608624898
    num_examples: 3698
  - name: 1p_train_v1
    num_bytes: 744588.0238671299
    num_examples: 524
  - name: 1p_train_v2
    num_bytes: 741746.0848447363
    num_examples: 522
  - name: 1p_train_v3
    num_bytes: 822741.3469829547
    num_examples: 579
  - name: 5p_train_v1
    num_bytes: 3880667.735078496
    num_examples: 2731
  - name: 5p_train_v2
    num_bytes: 3913350.0338360225
    num_examples: 2754
  - name: 5p_train_v3
    num_bytes: 3806777.3204962616
    num_examples: 2679
  - name: 10p_train_v1
    num_bytes: 7786912.921358534
    num_examples: 5480
  - name: 10p_train_v2
    num_bytes: 7785491.951847338
    num_examples: 5479
  - name: 10p_train_v3
    num_bytes: 7691707.964108348
    num_examples: 5413
  download_size: 6875945
  dataset_size: 145293067.4987746
---
# Dataset Card for "icdst_multiwoz_turns_v24"

[More Information needed](https://github.com/huggingface/datasets/blob/main/CONTRIBUTING.md#how-to-contribute-to-the-dataset-cards)